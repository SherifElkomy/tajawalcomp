package com.tajawal.task.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public class HotelModel implements Serializable {

    @SerializedName("hotelId")
    @Expose
    public int hotelIdResponse = 0;

    @SerializedName("image")
    @Expose
    public ArrayList<ImageHotelModel> imageResponse;

    @SerializedName("location")
    @Expose
    public LocationHotelModel locationHotelResponse;


    @SerializedName("summary")
    @Expose
    public SummaryHotelModel summaryHotelResponse;


}
