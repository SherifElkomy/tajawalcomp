package com.tajawal.task.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by P_BMB on 16-Feb-18.
 */

public class ImageHotelModel implements Serializable {
    @SerializedName("url")
    @Expose
    public String urlImageHotel = "";
}
