package com.tajawal.task.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public class LocationHotelModel implements Serializable {

    @SerializedName("address")
    @Expose
    public String addressResponse = "";

    @SerializedName("latitude")
    @Expose
    public double latitudeResponse = 0.0;


    @SerializedName("longitude")
    @Expose
    public double longitudeResponse = 0.0;

}
