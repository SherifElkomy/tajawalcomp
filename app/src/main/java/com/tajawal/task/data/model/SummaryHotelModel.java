package com.tajawal.task.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public class SummaryHotelModel implements Serializable {

    @SerializedName("highRate")
    @Expose
    public double highRateResponse = 0.0;

    @SerializedName("hotelName")
    @Expose
    public String hotelNameResponse = "";

    @SerializedName("lowRate")
    @Expose
    public double lowRateResponse = 0.0;

}
