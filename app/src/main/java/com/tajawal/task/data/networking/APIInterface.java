package com.tajawal.task.data.networking;


import com.tajawal.task.data.model.HotelModel;
import com.tajawal.task.data.model.HotelResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by anupamchugh on 09/01/17.
 */

public interface APIInterface {

    @POST("/tajawal/hotels_exercise.json")
  //  Call<ArrayList<ResponseHotelListModel>> getHotelsList();
//    Observable<List<HotelModel>> getHotelsList();
    Observable<Object> getHotelsList();
}
