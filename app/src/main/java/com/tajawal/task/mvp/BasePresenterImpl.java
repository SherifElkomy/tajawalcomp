package com.tajawal.task.mvp;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public class BasePresenterImpl<ViewT> implements IBasePresenter<ViewT> {
    protected ViewT mViewT;

    @Override
    public void onViewActive(ViewT viewT) {
        mViewT = viewT;
    }

    @Override
    public void onViewInActive() {
        mViewT = null;
    }
}
