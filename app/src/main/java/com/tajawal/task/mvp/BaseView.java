package com.tajawal.task.mvp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tajawal.task.R;

import butterknife.BindView;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public class BaseView extends Fragment implements IBaseView {

    @BindView(R.id.progressBar)
    protected ProgressBar mProgressBar;

    public  <T extends Fragment> void showFragment(Fragment fragment, Class<T> fragmentClass, Bundle bundle,
                                                   boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

//        if (fragment == null) {
//            try {
//                fragment = fragmentClass.newInstance();
//                fragment.setArguments(bundle);
//            } catch (InstantiationException e) {
//                throw new RuntimeException("New Fragment should have been created", e);
//            } catch (IllegalAccessException e) {
//                throw new RuntimeException("New Fragment should have been created", e);
//            }
//        }

        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right,
                R.anim.slide_out_left, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.fragmentPlaceHolder, fragment,
                fragmentClass.getSimpleName());


        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setProgressBar(boolean show) {
        if (show) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
