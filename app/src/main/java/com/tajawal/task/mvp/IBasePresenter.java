package com.tajawal.task.mvp;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public interface IBasePresenter<ViewT> {
    void onViewActive(ViewT viewT);
    void onViewInActive();
}
