package com.tajawal.task.mvp;

import android.content.Context;

/**
 * Created by P_BMB on 15-Feb-18.mCompelexPref = ComplexPreferences.getComplexPreferences(this.getActivity(), "prefandroid", MODE_PRIVATE);
 */

public interface IBaseView {
    void showToastMessage(String message);
    void setProgressBar(boolean show);
    Context getContext();
}
