package com.tajawal.task.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tajawal.task.R;
import com.tajawal.task.mvp.IBaseView;

import butterknife.BindView;

/**
 * Created by P_BMB on 16-Feb-18.
 */

public class BaseActivity extends AppCompatActivity implements IBaseView {


    protected ProgressBar mProgressBar;

    public  <T extends Fragment> void showFragment(Fragment fragment, Class<T> fragmentClass, Bundle bundle,
                                                  boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

//        if (fragment == null) {
//            try {
//                fragment = fragmentClass.newInstance();
//                fragment.setArguments(bundle);
//            } catch (InstantiationException e) {
//                throw new RuntimeException("New Fragment should have been created", e);
//            } catch (IllegalAccessException e) {
//                throw new RuntimeException("New Fragment should have been created", e);
//            }
//        }

        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right,
                R.anim.slide_out_left, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.fragmentPlaceHolder, fragment,
                fragmentClass.getSimpleName());


        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setProgressBar(boolean show) {
        if(mProgressBar == null){
            mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        }
        if (show) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }
}
