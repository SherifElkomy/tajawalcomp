package com.tajawal.task.ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tajawal.task.R;
import com.tajawal.task.data.model.HotelModel;
import com.tajawal.task.ui.listinghotels.HotelContract;
import com.tajawal.task.ui.listinghotels.HotelPresenter;
import com.tajawal.task.ui.listinghotels.HotelsListFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements HotelContract.View{

    private HotelContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new HotelPresenter(this);


    }



    @Override
    protected void onResume() {
        super.onResume();
        presenter.getHotels(MainActivity.this);
    }

    @Override
    public void loadHotels(ArrayList<HotelModel> hotels) {
        Fragment mFragment = new HotelsListFragment();
        showFragment(mFragment, HotelsListFragment.class, null, false);
    }
}
