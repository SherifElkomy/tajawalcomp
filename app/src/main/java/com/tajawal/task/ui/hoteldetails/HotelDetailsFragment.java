package com.tajawal.task.ui.hoteldetails;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tajawal.task.R;
import com.tajawal.task.data.model.HotelModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelDetailsFragment extends Fragment {

    HotelModel mHotelObject;
    private SupportMapFragment mSupportMapFragment;
    ImageView mImageHotel;
    TextView mHotelName, mHotelHighRate, mHotelLowRate;
    GoogleMap mGoogleMap;
    boolean isImageFitToScreen = true;


    public HotelDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hotel_details, container, false);
        Bundle bundle = this.getArguments();
        mHotelObject = (HotelModel) bundle.getSerializable("serializObjectOrder");
        initView(view);
        setupMap();

        Glide.with(HotelDetailsFragment.this.getActivity())
                .load(mHotelObject.imageResponse.get(0).urlImageHotel)
                .centerCrop()
                .into(new GlideDrawableImageViewTarget(mImageHotel) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        //viewHolder.ivPhoto.setImageResource(R.mipmap.ic_launcher);

                    }
                });

        RelativeLayout.LayoutParams mReltvLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 200);
        mReltvLayout.setMargins(5, 5, 5, 0);
        mImageHotel.setLayoutParams(mReltvLayout);
        mImageHotel.setAdjustViewBounds(true);

        mImageHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isImageFitToScreen == false) {
                    isImageFitToScreen=true;
                    RelativeLayout.LayoutParams mReltvLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 200);
                    mReltvLayout.setMargins(5, 5, 5, 0);
                    mImageHotel.setLayoutParams(mReltvLayout);
                   mImageHotel.setAdjustViewBounds(true);
                }else{
                    isImageFitToScreen=false;
                    mImageHotel.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                    mImageHotel.setScaleType(ImageView.ScaleType.FIT_XY);
                }

            }
        });

        mHotelName.setText(mHotelObject.summaryHotelResponse.hotelNameResponse);
        mHotelHighRate.setText(String.valueOf(mHotelObject.summaryHotelResponse.highRateResponse));
        mHotelLowRate.setText(String.valueOf(mHotelObject.summaryHotelResponse.lowRateResponse));

        return view;
    }

    private void initView(View view) {
        mImageHotel = (ImageView)view.findViewById(R.id.hotelImg);
        mHotelName = (TextView)view.findViewById(R.id.hotelNameDetails);
        mHotelHighRate = (TextView)view.findViewById(R.id.hotelHieghDetails);
        mHotelLowRate = (TextView)view.findViewById(R.id.hotelLowDetails);
    }


    public void setupMap() {

        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapLocationHotel);
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.mapLocationHotel, mSupportMapFragment).commit();
        }

        if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    if (googleMap != null) {

                        googleMap.getUiSettings().setAllGesturesEnabled(true);
                        mGoogleMap = googleMap;
//                      -> marker_latlng // MAKE THIS WHATEVER YOU WANT


                        LatLng mHotelLatLng = new LatLng(mHotelObject.locationHotelResponse.latitudeResponse, mHotelObject.locationHotelResponse.longitudeResponse);
                        mGoogleMap.addMarker(new MarkerOptions().position(mHotelLatLng).title("hotel"));
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mHotelLatLng, 17.0f));

                        if (ActivityCompat.checkSelfPermission(HotelDetailsFragment.this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HotelDetailsFragment.this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling

                            return;
                        }
                       // googleMap.setMyLocationEnabled(true);
                        Location mHotelLocation = new Location("locationHotel");
                        mHotelLocation.setLatitude(mHotelObject.locationHotelResponse.latitudeResponse);
                        mHotelLocation.setLongitude(mHotelObject.locationHotelResponse.longitudeResponse);



                    }

                }
            });


        }
    }

}
