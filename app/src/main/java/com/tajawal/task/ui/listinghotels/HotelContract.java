package com.tajawal.task.ui.listinghotels;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.tajawal.task.data.model.HotelModel;
import com.tajawal.task.mvp.IBasePresenter;
import com.tajawal.task.mvp.IBaseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public interface HotelContract {

    interface View extends IBaseView{
        void loadHotels(ArrayList<HotelModel> hotels);
    }

   interface Presenter extends IBasePresenter<View>{
        void getHotels(Context context);
   }




}
