package com.tajawal.task.ui.listinghotels;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tajawal.task.data.model.HotelModel;
import com.tajawal.task.data.model.HotelResponse;
import com.tajawal.task.data.model.ListHotelModel;
import com.tajawal.task.data.networking.APIClient;
import com.tajawal.task.data.networking.APIInterface;
import com.tajawal.task.data.pref.ComplexPreferences;
import com.tajawal.task.mvp.BasePresenterImpl;
import com.tajawal.task.ui.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by P_BMB on 15-Feb-18.
 */

public class HotelPresenter extends BasePresenterImpl<HotelContract.View> implements HotelContract.Presenter {

    CompositeDisposable mComposibleDisposable;
    private APIInterface mApiInterface;
    ComplexPreferences complexPreferences;
    ArrayList<HotelModel> hotelArrayItems;

    public HotelPresenter() {

    }

    public HotelPresenter(HotelContract.View view) {
        onViewActive(view);
        mApiInterface = new APIClient().getHotelsService();
        mComposibleDisposable = new CompositeDisposable();
    }

    @Override
    public void getHotels(final Context context) {

        mComposibleDisposable.add(mApiInterface.getHotelsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Object>() {
                    @Override
                    public void onNext(Object objectItem) {
                        String v = objectItem.toString();
                        Map<String, ArrayList<Object>> map = (Map<String, ArrayList<Object>>) objectItem;
                        ArrayList<Object> arrayItemObject = map.get("hotel");
                        JSONObject jsonObject = new JSONObject(map);

                        Gson gson = new Gson();
                        try {
                            //Steps to convert JSONArray to ArrayList of My model "HotelModel"
                            JSONArray jsonArrayObject = jsonObject.getJSONArray("hotel");
                            Type type = new TypeToken<ArrayList<HotelModel>>() {
                            }.getType();
                            hotelArrayItems = gson.fromJson(jsonArrayObject.toString(), type);
                            int sizeArray = hotelArrayItems.size();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }catch (Exception e){
                            String error = e.getMessage();
                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        String error = e.getMessage();
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete() {
                        complexPreferences = ComplexPreferences.getComplexPreferences(context, "prefandroid", MODE_PRIVATE);
                        ListHotelModel listHotelModelObject = new ListHotelModel();
                        listHotelModelObject.itemModel = hotelArrayItems;
                        complexPreferences.putObject("ListHotelModel", listHotelModelObject);
                        complexPreferences.commit();

                        HotelContract.View iteViewCallBack = (HotelContract.View)(BaseActivity)context;
                        iteViewCallBack.loadHotels(hotelArrayItems);

                    }
                }));


    }
}
