package com.tajawal.task.ui.listinghotels;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.tajawal.task.R;
import com.tajawal.task.data.model.HotelModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Created by P_BMB on 16-Feb-18.
 */

public class HotelsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

public ArrayList<HotelModel> mHotels;
public Fragment fragment;




public HotelsAdapter(Fragment fragment, ArrayList<HotelModel> hotels) {
        this.fragment = fragment;
        this.mHotels = hotels;
        }

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView tvTitle;
    public TextView tvHigh;
    public TextView tvLow;
    public ImageView ivPhoto;

    public ViewHolder(View itemView) {
        super(itemView);
        ivPhoto = (ImageView)itemView.findViewById(R.id.imageHotel);
        tvTitle = (TextView)itemView.findViewById(R.id.hotelName);
        tvHigh = (TextView)itemView.findViewById(R.id.textHieghRate);
        tvLow = (TextView)itemView.findViewById(R.id.textLowRate);
    }
}

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder viewHolderItemObject = (ViewHolder)viewHolder;
        HotelModel hotels = mHotels.get(position);

        viewHolderItemObject.tvTitle.setText(hotels.summaryHotelResponse.hotelNameResponse);
        viewHolderItemObject.tvHigh.setText(String.valueOf(hotels.summaryHotelResponse.highRateResponse));
        viewHolderItemObject.tvLow.setText(String.valueOf(hotels.summaryHotelResponse.lowRateResponse));
        Glide.with(fragment.getActivity())
                .load(hotels.imageResponse.get(0).urlImageHotel)
                .into(new GlideDrawableImageViewTarget(viewHolderItemObject.ivPhoto) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        //viewHolder.ivPhoto.setImageResource(R.mipmap.ic_launcher);

                    }
                });

    }

    @Override
    public int getItemCount() {
        return mHotels.size();
    }

    public void clear() {
        int size = getItemCount();
        mHotels.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(List<HotelModel> photos) {
        int prevSize = getItemCount();
        this.mHotels.addAll(photos);
        notifyItemRangeInserted(prevSize, photos.size());
    }
}

