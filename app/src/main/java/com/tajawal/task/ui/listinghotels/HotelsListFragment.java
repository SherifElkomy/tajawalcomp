package com.tajawal.task.ui.listinghotels;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tajawal.task.R;
import com.tajawal.task.data.model.HotelModel;
import com.tajawal.task.data.model.ListHotelModel;
import com.tajawal.task.data.pref.ComplexPreferences;
import com.tajawal.task.mvp.BaseView;
import com.tajawal.task.ui.BaseActivity;
import com.tajawal.task.ui.MainActivity;
import com.tajawal.task.ui.hoteldetails.HotelDetailsFragment;
import com.tajawal.task.util.AppConstant;
import com.tajawal.task.util.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelsListFragment extends BaseView implements HotelContract.View {


    RecyclerView mHotelList;
    ComplexPreferences mCompelexPref;
    ListHotelModel mListHotelModel;
    HotelsAdapter mAdapterHotels;

    public HotelsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hotels_list, container, false);
        ButterKnife.bind(this, view);
      //  presenter = new HotelPresenter(this);
        mCompelexPref = ComplexPreferences.getComplexPreferences(this.getActivity(), "prefandroid", MODE_PRIVATE);

        initView(view);



        mHotelList.addOnItemTouchListener(
                new RecyclerItemClickListener(HotelsListFragment.this.getActivity(), mHotelList ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        HotelModel hotelObject = mListHotelModel.itemModel.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("serializObjectOrder", hotelObject);

                        Fragment hotelDetails = new HotelDetailsFragment();
                        hotelDetails.setArguments(bundle);
                        showFragment(hotelDetails, HotelDetailsFragment.class, null, true);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return view;
    }

    private void initView(View view) {
        mHotelList = (RecyclerView)view.findViewById(R.id.hotelList);
    }


    @Override
    public void onResume() {
        super.onResume();


    }


    @Override
    public void onActivityCreated(@android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListHotelModel = mCompelexPref.getObject("ListHotelModel", ListHotelModel.class);
        if(mListHotelModel != null) {
            mAdapterHotels = new HotelsAdapter(HotelsListFragment.this, mListHotelModel.itemModel);
            mHotelList.setLayoutManager(new LinearLayoutManager(getActivity()));
            mHotelList.setAdapter(mAdapterHotels);
        }
        mHotelList.getLayoutManager().onRestoreInstanceState(AppConstant.listState);
    }


    @Override
    public void onPause() {
        super.onPause();

        RecyclerView.LayoutManager mLayoutManag = mHotelList.getLayoutManager();
        if(mLayoutManag != null) {
            AppConstant.listState = mHotelList.getLayoutManager().onSaveInstanceState();
        }
    }

    @Override
    public void loadHotels(ArrayList<HotelModel> hotels) {
        mAdapterHotels = new HotelsAdapter(HotelsListFragment.this, hotels);
    }
}
